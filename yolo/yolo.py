from keras import backend as K
from keras.models import load_model
from timeit import default_timer as timer
from PIL import ImageDraw, Image
from yolo.model import eval
from src.utils.preprocessor import preprocess_input
from os.path import join
from os import listdir
from keras_models import *

import os
import colorsys
import numpy as np
import cv2
import shutil
import pickle
import datetime
import math

import face_recognition
import tensorflow as tf

tf.compat.v1.disable_eager_execution()

FRAMES_DIR = "outputs/frames"
FACES_DIR = "outputs/faces"
VIDEOS_DIR = "outputs/out_videos"


def delete_folder_files(path):
    shutil.rmtree(path, ignore_errors=True)
    return


def check_exist_path(path):
    if not os.path.exists(path):
        os.mkdir(path)
    #     else:
    #         delete_folder_files(path)
    #         os.mkdir(path)


# def get_known_face_names_encodings():
#     _known_face_encodings = []
#     _known_face_names = []
#
#     for f in listdir(FACES_DIR):
#         image = face_recognition.load_image_file(join(FACES_DIR, f))
#         enc = face_recognition.face_encodings(image)
#         if len(enc) == 0:
#             continue
#         face_encoding = enc[0]
#         _known_face_encodings.append(face_encoding)
#         _known_face_names.append(f.split('.')[0])
#
#     return _known_face_names, _known_face_encodings
#
#
# def register_new_face(_face_encoding, _name):
#     known_face_encodings.append(_face_encoding)
#     known_face_names.append(_name)


check_exist_path(FRAMES_DIR)
check_exist_path(FACES_DIR)
check_exist_path(VIDEOS_DIR)


# known_face_names, known_face_encodings = get_known_face_names_encodings()

WRITE_VIDEO = True

nameStatus = {}
nameLastUpdate = {}
enteringTime = {}

now = datetime.datetime.now()

logfilename = str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '_logs.txt'
print(logfilename)
logfile = open(logfilename, 'w')


################################


class YOLO(object):
    def __init__(self, args):
        self.args = args
        self.model_path = args.model
        self.classes_path = args.classes
        self.anchors_path = args.anchors
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = tf.compat.v1.keras.backend.get_session()
        self.boxes, self.scores, self.classes = self._generate()
        self.model_image_size = args.img_size

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def _generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file'

        # load model, or construct model and load weights
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except Exception as err:
            print(err)
            # make sure model, anchors and classes match
            self.yolo_model.load_weights(self.model_path)
        else:
            # Mismatch between model and given anchor and class sizes
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                   num_anchors / len(self.yolo_model.output) * (
                           num_classes + 5)
        self.yolo_model._make_predict_function()
        print('*** {} model, anchors, and classes loaded.'.format(model_path))

        # generate colors for drawing bounding boxes
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))

        # shuffle colors to decorrelate adjacent classes.
        np.random.seed(102)
        np.random.shuffle(self.colors)
        np.random.seed(None)

        # generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2,))
        boxes, scores, classes = eval(self.yolo_model.output, self.anchors,
                                      len(self.class_names),
                                      self.input_image_shape,
                                      score_threshold=self.args.score,
                                      iou_threshold=self.args.iou)
        return boxes, scores, classes

    def detect_image(self, image):
        def get_yolo_model_input(_image):
            if self.model_image_size != (None, None):
                assert self.model_image_size[0] % 32 == 0, 'Multiples of 32 required'
                assert self.model_image_size[1] % 32 == 0, 'Multiples of 32 required'
                boxed_image = letterbox_image(_image, tuple(
                    reversed(self.model_image_size)))
            else:
                new_image_size = (_image.width - (_image.width % 32),
                                  _image.height - (_image.height % 32))
                boxed_image = letterbox_image(_image, new_image_size)
            _image_data = np.array(boxed_image, dtype='float32')
            print(_image_data.shape)
            _image_data /= 255.
            # add batch dimension
            _image_data = np.expand_dims(_image_data, 0)
            return _image_data

        start_time = timer()

        image_data = get_yolo_model_input(image)

        out_boxes, out_scores, out_classes = self.sess.run(
            [self.boxes, self.scores, self.classes],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: [image.size[1], image.size[0]],
                K.learning_phase(): 0
            })
        print('*** Found {} face(s) for this image'.format(len(out_boxes)))
        thickness = (image.size[0] + image.size[1]) // 400

        for i, c in reversed(list(enumerate(out_classes))):
            predicted_class = self.class_names[c]
            box = out_boxes[i]
            score = out_scores[i]
            text = '{} {:.2f}'.format(predicted_class, score)
            draw = ImageDraw.Draw(image)

            top, left, bottom, right = box
            top = max(0, np.floor(top + 0.5).astype('int32'))
            left = max(0, np.floor(left + 0.5).astype('int32'))
            bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
            right = min(image.size[0], np.floor(right + 0.5).astype('int32'))

            # print(text, (left, top), (right, bottom))

            for thk in range(thickness):
                draw.rectangle(
                    [left + thk, top + thk, right - thk, bottom - thk],
                    outline=(51, 178, 255), width=1)
            del draw

        end_time = timer()
        print('*** Processing time: {:.2f}ms'.format((end_time -
                                                      start_time) * 1000))
        return image, out_boxes

    def close_session(self):
        self.sess.close()


def letterbox_image(image, size):
    """Resize image with unchanged aspect ratio using padding"""
    img_width, img_height = image.size
    w, h = size
    scale = min(w / img_width, h / img_height)
    nw = int(img_width * scale)
    nh = int(img_height * scale)

    image = image.resize((nw, nh), Image.BICUBIC)
    new_image = Image.new('RGB', size, (128, 128, 128))
    new_image.paste(image, ((w - nw) // 2, (h - nh) // 2))
    return new_image


def detect_img(yolo):
    while True:
        img = input('*** Input image filename: ')
        try:
            image = Image.open(img)
        except Exception as err:
            print(err)
            if img == 'q' or img == 'Q':
                break
            else:
                print('*** Open Error! Try again!')
                continue
        else:
            res_image, _ = yolo.detect_image(image)
            res_image.show()
    yolo.close_session()


def get_emotion():
    pass


def get_genders():
    pass


def get_age():
    pass


def get_temp():
    pass


def get_time_in():
    pass


def save_face(image, face_n, locations, margin=20):
    """

    :param margin:
    :param image:
    :param face_n:
    :param locations:
    :return:
    """
    top, left, bottom, right = locations
    # top, right, bottom, left = face_location

    img_h, img_w, _ = np.shape(image)
    top = max(int(top - margin), 0)
    left = max(int(left - margin), 0)
    bottom = min(int(bottom + margin), img_h - 1)
    right = min(int(right + margin), img_w - 1)
    face_image = image[top:bottom, left:right]

    face_image = cv2.resize(src=face_image, dsize=(128, 128), interpolation=cv2.INTER_LINEAR)
    cv2.imwrite(join(FACES_DIR, face_n + '.jpg'), face_image, [cv2.IMWRITE_JPEG_QUALITY, 100])


def load_data(file_n='encodings.pickle'):
    try:
        with open(file_n, 'rb') as file:
            _data = pickle.load(file)
        print("Encoding data has been loaded...")

    except (OSError, IOError) as err:
        print("Create empty encoding data...\n")
        _data = {"encodings": [], "names": []}
        with open(file_n, 'wb') as file:
            pickle.dump(_data, file)
    return _data


def write_data(_data, file_n='encodings.pickle'):
    with open(file_n, 'wb') as file:
        pickle.dump(_data, file)


data = load_data()
face_names = []


def frame_process(frame, locations):
    """
    :param frame:
    :param locations:
    :return:
    """
    encodings = face_recognition.face_encodings(frame, locations)
    global face_names

    for location, encoding in zip(locations, encodings):
        matches = face_recognition.compare_faces(data["encodings"], encoding, tolerance=0.2)
        known_encodings = len(data["encodings"])

        if True in matches:
            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
            counts = {}
            for i in matchedIdxs:
                name = data["names"][i]
                counts[name] = counts.get(name, 0) + 1
            name = max(counts, key=counts.get)
        else:
            name = str(known_encodings + 1)
            # print("[INFO] serializing encodings...")
            print(f"[INFO] detected person, name: {name}")
            data["encodings"].append(encoding)
            data["names"].append(name)
            # Save
            write_data(data)
            save_face(frame, name, location)

        face_names.append(name)

        # matches = face_recognition.compare_faces(known_face_encodings, face_encoding, tolerance=0.9)
        #
        # if True in matches:
        #     first_match_index = matches.index(True)
        #     face_n = known_face_names[first_match_index]
        #     continue

        # face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
        # if face_distances != []:
        #     best_match_index = np.argmin(face_distances)
        #     best_distance = face_distances[best_match_index]
        #     if best_distance < 0.2:
        #         print(f"Detected known face, index = {best_match_index}")
        #         face_n = known_face_names[best_match_index]
        #         continue

        # path = f"{join(FACES_DIR, face_n)}

        # register_new_face(face_encoding, face_n)


def detect_video(model, video_path=None):
    if video_path == 'stream':
        vid = cv2.VideoCapture(0)
    else:
        vid = cv2.VideoCapture(video_path)
    if not vid.isOpened():
        raise IOError("Couldn't open webcam or video")

    # the video format and fps
    # video_fourcc = int(vid.get(cv2.CAP_PROP_FOURCC))
    video_fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
    video_fps = vid.get(cv2.CAP_PROP_FPS)

    # the size of the frames to write
    video_size = (int(vid.get(cv2.CAP_PROP_FRAME_WIDTH)),
                  int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    if WRITE_VIDEO:
        output_fn = "output_video.avi"
        out = cv2.VideoWriter(join(VIDEOS_DIR, output_fn), video_fourcc, video_fps, video_size)

    accum_time = 0
    index = 0
    curr_fps = 0
    fps = "FPS: ??"
    prev_time = timer()
    global face_names
    ###################################
    while True:
        ret, frame = vid.read()
        if not ret:
            break

        index += 1
        face_names = []

        image = Image.fromarray(frame)
        input_img = np.asarray(Image.fromarray(frame))
        gray_image = np.asarray(Image.fromarray(frame).convert('L'))

        # input_img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        image, faces = model.detect_image(image)
        result = np.asarray(image)
        # faces = face_recognition.face_locations(frame)

        frame_process(frame=frame, locations=faces)

        for (top, left, bottom, right), name in zip(faces, face_names):
            gray_face = gray_image[int(top):int(bottom), int(left):int(right)]
            height, width = np.shape(gray_face)
            expanded_face = get_expanded_face(input_img, top, bottom, left, right, width, height)
            try:
                gray_face = cv2.resize(gray_face, emotion_target_size)
            except Exception as e:
                print(e)
                continue

            gray_face = preprocess_input(gray_face, True)
            gray_face = np.expand_dims(gray_face, 0)
            gray_face = np.expand_dims(gray_face, -1)

            emotion_prediction = emotion_classifier.predict(gray_face)
            emotion_probability = np.max(emotion_prediction)
            emotion_label_arg = np.argmax(emotion_probability)
            emotion_text = emotion_labels[emotion_label_arg]

            results = gen_age_model.predict(np.array([expanded_face]))
            predicted_genders = "Woman" if results[0][0][0] > 0.5 else "Man"
            ages = np.arange(0, 101).reshape(101, 1)
            predicted_ages = int(results[1].dot(ages).flatten()[0])

        for (top, left, bottom, right), name in zip(faces, face_names):
            print(name, ' detected')
            currentTime = datetime.datetime.now()

            if name not in nameStatus:
                logfile.write(
                    name + ' has entered at - ' + str(currentTime.hour) + ':' + str(currentTime.minute) + ':' + str(
                        currentTime.second) + '\n')
                print(name + ' has entered')
                nameStatus[name] = 'IN'
                nameLastUpdate[name] = currentTime
                enteringTime[name] = currentTime

            else:
                lastUpdateTime = nameLastUpdate[name]
                difference = currentTime - lastUpdateTime
                if difference.seconds < 10:
                    print('last_update_time_too_close : not changing status')
                else:
                    if nameStatus[name] == 'IN':
                        timein = currentTime - enteringTime[name]
                        minutesIn = math.floor(timein.seconds / 60)
                        secondsIn = timein.seconds % 60
                        logfile.write(
                            name + ' has left at - ' + str(currentTime.hour) + ':' + str(
                                currentTime.minute) + ':' + str(
                                currentTime.second))
                        logfile.write(
                            '...........Duration - ' + str(minutesIn) + ' mins ' + str(secondsIn) + 'secs \n')
                        print(name + ' has left.')
                        nameStatus[name] = 'OUT'
                    else:
                        logfile.write(
                            name + ' has entered at - ' + str(currentTime.hour) + ':' + str(
                                currentTime.minute) + ':' + str(
                                currentTime.second) + '\n')
                        print(name + ' has entered.')
                        nameStatus[name] = 'IN'
                        enteringTime[name] = currentTime

                nameLastUpdate[name] = currentTime

            # Drawing rectangle
            # Drawing rectangle
            # cv2.rectangle(result, (left, top), (right, bottom), (0, 255, 0), 2)
            # initialize the set of information we'll displaying on the frame
            cv2.rectangle(result, (5, 5), (120, 50), (0, 0, 0), cv2.FILLED)

            y = top - 15 if top - 15 > 15 else top + 15
            cv2.putText(result, name, (int(left), int(y)), cv2.FONT_HERSHEY_SIMPLEX,
                        0.6, (0, 255, 0), 2)

            # top *= 4
            # right *= 4
            # bottom *= 4
            # left *= 4

        curr_time = timer()
        exec_time = curr_time - prev_time
        prev_time = curr_time
        accum_time = accum_time + exec_time
        curr_fps = curr_fps + 1
        if accum_time > 1:
            accum_time = accum_time - 1
            fps = curr_fps
            curr_fps = 0

        # # initialize the set of information we'll displaying on the frame
        # info = [
        #     ('FPS', '{}'.format(fps)),
        #     ('Faces detected', '{}'.format(len(faces)))
        # ]
        # cv2.rectangle(result, (5, 5), (120, 50), (0, 0, 0), cv2.FILLED)
        #
        # for (i, (txt, val)) in enumerate(info):
        #     text = '{}: {}'.format(txt, val)
        #     cv2.putText(result, text, (10, (i * 20) + 20),
        #                 cv2.FONT_HERSHEY_SIMPLEX, 0.3, (10, 175, 0), 1)

        cv2.imwrite(join(FRAMES_DIR, f"frame{index}.jpg"), result, [cv2.IMWRITE_JPEG_QUALITY, 100])

        if WRITE_VIDEO:
            out.write(result)

    vid.release()
    out.release()
    # close the session
    model.close_session()
