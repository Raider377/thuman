# from src.utils.preprocessor import preprocess_input
from src.utils.datasets import get_labels
from keras.models import load_model
from keras.utils.data_utils import get_file
from wide_resnet import WideResNet
import os
import cv2
import numpy as np
import tensorflow as tf
tf.compat.v1.disable_v2_behavior()

emotion_model_path = 'trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')
emotion_classifier = load_model(emotion_model_path, compile=False)
emotion_classifier._make_predict_function()
emotion_target_size = emotion_classifier.input_shape[1:3]
emotion_offsets = (20, 40)

# ages and genders
CASE_PATH = "./models/haarcascade_frontalface_alt.xml"
WRN_WEIGHTS_PATH = "https://github.com/Tony607/Keras_age_gender/releases/download/V1.0/weights.18-4.06.hdf5"
gen_age_model = WideResNet(64, depth=16, k=8)()
model_dir = os.path.join(os.getcwd(), "trained_models").replace("//", "\\")
fpath = get_file('weights.18-4.06.hdf5',
                 WRN_WEIGHTS_PATH,
                 cache_subdir=model_dir)
gen_age_model.load_weights(fpath)
gen_age_model._make_predict_function()


def get_expanded_face(img, top, bottom, left, right, width, height):
    img_h, img_w, _ = np.shape(img)
    xw1 = max(int(left - 0.4 * width), 0)
    yw1 = max(int(top - 0.4 * height), 0)
    xw2 = min(int(right + 0.4 * width), img_w - 1)
    yw2 = min(int(bottom + 0.4 * height), img_h - 1)
    return cv2.resize(img[yw1:yw2 + 1, xw1:xw2 + 1, :], (128, 128))
