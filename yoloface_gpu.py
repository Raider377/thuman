import argparse
from yolo.yolo import YOLO, detect_video
from tensorflow.python.client import device_lib
from deepface import DeepFace


#####################################################################
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, default='model-weights/YOLO_Face.h5',
                        help='path to model weights file')
    parser.add_argument('--anchors', type=str, default='cfg/yolo_anchors.txt',
                        help='path to anchor definitions')
    parser.add_argument('--classes', type=str, default='cfg/face_classes.txt',
                        help='path to class definitions')
    parser.add_argument('--score', type=float, default=0.4,
                        help='the score threshold')
    parser.add_argument('--iou', type=float, default=0.4,
                        help='the iou threshold')
    parser.add_argument('--img-size', type=list,
                        default=(832, 832), help='input image size')
    parser.add_argument('--video', type=str, default='videos/1F-02_20210904-150000--20210904-160000.avi',
                        help='path to the video')
    parser.add_argument('--output', type=str, default='outputs/',
                        help='image/video output path')
    args = parser.parse_args()
    return args


def get_available_devices():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos]


def _main():
    # Get the arguments
    args = get_args()
    print(get_available_devices())
    print('[i] ==> Video detection mode\n')

    backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface']
    result = DeepFace.verify("outputs/faces/9.jpg", "outputs/faces/13.jpg", detector_backend=backends[4],
                             enforce_detection=False)  # validate our images
    print(result)

    # Call the detect_video method here
    # detect_video(YOLO(args), args.video)
    print('Well done!!!')


if __name__ == "__main__":
    _main()
